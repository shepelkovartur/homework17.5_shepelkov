
#include <iostream>

class Vector
{
public:
	Vector() : x(5), y(5), z(5)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
		std::cout << '\n' << sqrt(x * x + y * y + z * z);
	}
private:
	double x;
	double y;
	double z;
};

int main()
{
	Vector v;
	v.Show();
}